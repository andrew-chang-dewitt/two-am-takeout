## Fix failing test

handlers.HealthBody has field named `OK`, but frontend expects JSON response from `/health` to have property `ok`.
Unable to change field name to start with lowercase 'o' as it wouldn't be exported then, but instead changed json tag on the field to match `ok`.


## Fix test assertions

Frontend expects all messages passed throw /ws socket to be json, but tests only checked if bytes in message received can be represented as a string.
Changed tests to use ReadJSON & WriteJSON methods so that tests mirror communication that frontend expects.

My concerns about this are that while the test does prove a message can be sent & received as JSON, the server doesn't require messages to be JSON encoded & the test doesn't show that the server may also transmit byte-encoded messages.
I didn't have time to further explore restricting messages to being JSON encoded by the server, however, so the tests & server were left with only the ReadJSON & WriteJSON changes.

Finally, this one was surprisingly hard for me for some reason.
It wasn't until I took a break & walked away from the computer to do other things for a while before coming back to it that I realized what it was that I'd been missing.

## Add username to front end:

Adding username in component state, set by additional field, then send that username as a property on the message.
Guards against sending anonymous messages.
Ideally, it would show an alert to the user if they haven't set their username yet, but I ran out of time to implement that.

Displaying the name was more tricky.
As I'm not familiar with vuetify, it took some time to find the right directive to in the library to use so that the new features stay similar to the existing code for the chat message feature.
Ended up implementing it as a `v-badge` with a little `v-hover` to add some UI flair & keep it uncluttered unless the user wants to see the username.

## Security issue

I believe I found the security issue mentioned in the README.
From my understanding, the CheckOrigin method on the websocket.Upgrader created in ./server/sockets/client.go should be used to restrict connections to the websocket server since a browser will open a Websocket connection to any host.
I've updated the method to inspect the Origin Header on incoming requests and it only approves those that match "http://localhost:8080".

Of course, this broke the tests, so I had to update those to fake their origins as being the expected origin as well.

## Nicer looking frontend

In the end, I didn't have a ton of time to spend on this, and I'm pretty unfamiliar with Vuetify (I'm much more familiar with defining styles in CSS directly with CSS-Modules), so I didn't make many changes in terms of looks.
I do think the little transitions on the badges look kinda fun though, and they were surprisingly easy to implement with Vuetify.

Also, I'm not at all a fan of how I laid out the display name input field at all, but I ran out of time poking around Vuetify's docs to come up with a better solution.
